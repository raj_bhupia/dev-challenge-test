exports.sortObjectValues =  function (data){
	return Object.values(data).sort(function(a, b) {
		return a.lastChangeBid-b.lastChangeBid;
	});
}