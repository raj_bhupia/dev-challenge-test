const utils = require("./utils");
const Sparkline = require("../site/sparkline");

let tableData = {};
let tableHeader;
const SPARKLING_HEADER = 'sparkline';
let sparklineData = {}

exports.modifyTable = function(data){
	var name = JSON.parse(data).name;
	tableData[name] = JSON.parse(data);
	
	let dataArray = utils.sortObjectValues(tableData);
	if(!tableHeader) {	
		tableHeader =Object.keys(dataArray[0]);
		tableHeader.push(SPARKLING_HEADER);
		createTableHeader(tableHeader);
	}

	createTableBody(dataArray);
	createSparkline();
}

const createTableHeader = function(headers){
	//tableHeader = headers;
	var table = document.getElementById('table');
	table.innerHTML = '';
	var thead = getTableHeader(headers);
	table.insertBefore(thead, table.firstChild);
}


const getTableHeader = function(headers){
	var thead = document.createElement("thead");
	var row = createRow(headers, false);
	thead.appendChild(row);
	return thead;
}

const  createTableBody = function(data){
	var table = document.getElementById('table');
	if(table.firstChild != table.lastChild)
	table.lastChild.innerHTML = '';
	var tbody = document.createElement("tbody");
	for(index in data){
		var row = createRow(data[index], true);
		tbody.appendChild(row);
	}
	table.appendChild(tbody);
	//return tbody;
}

const  createCell =  function(value){
	var cell = document.createElement("td");
	var value = document.createTextNode(value);
	cell.appendChild(value);
	return cell;
}
const createRow =function(data, istBody){
	var tr = document.createElement("tr");
	var values = Object.values(data);
	for(index in values){
		var cell = createCell(values[index]);
		tr.appendChild(cell);
	}
	if(istBody){
			tr.appendChild(sparklineCell(data));
	}
	return tr;
}

const sparklineCell = function(data){
	var cell = document.createElement("td" );
	cell.id = 'sparkline'+data.name;  
	return cell; 
}

exports.updateSparkingData = function(){
	var currencyNames = Object.keys(tableData);
	for ( name in currencyNames) {
		var rowData = tableData[currencyNames[name]];
		var midPrice = rowData.bestBid+ rowData.bestAsk/2;
		if(!sparklineData[currencyNames[name]]){
			sparklineData[currencyNames[name]] = [midPrice]
		}else{
			sparklineData[currencyNames[name]].push(midPrice);
		}
	}
	createSparkline();
}
    createSparkline = function(){
	var sparklineNames = Object.keys(sparklineData);
	for(i in sparklineNames){
		
		createSparklineforSingleRecord(sparklineNames[i], sparklineData[sparklineNames[i]]);
	}
}
const createSparklineforSingleRecord = function(name,sparklineArray){
	var cell = document.getElementById("sparkline"+name );
	
	const sparkline = new Sparkline(cell);
    sparkline.draw(sparklineArray);
}

