var events = {}

exports.dispatch = function(eventName, data) {
	var callback = events[eventName];

	callback(data)
	
}
exports.register = function(eventName, callback){
	events[eventName] = callback;
}