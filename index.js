/**
 * This javascript file will constitute the entry point of your solution.
 *
 * Edit it as you need.  It currently contains things that you might find helpful to get started.
 */

// This is not really required, but means that changes to index.html will cause a reload.
require('./site/index.html')
// Apply the styles in style.css to the page.
require('./site/style.css')


// if you want to use es6, you can do something like
var eventHandler = require('./es6/eventHandler.js')
var table = require('./es6/table.js')
// here to load the myEs6code.js file, and it will be automatically transpiled.
// Change this to get detailed logging from the stomp library
global.DEBUG = false

const url = "ws://localhost:8011/stomp"
const client = Stomp.client(url)
client.debug = function(msg) {
  if (global.DEBUG) {
    console.info(msg)
  }
}

Object.values = function(data) {
	var dataArray = new Array;
	for(var o in data) {
	    dataArray.push(data[o]);
	}
	return dataArray;
};

var tableData = {};
eventHandler.register('wsData', connectCallback);
function connectCallback(data) {

	table.modifyTable(data);
  //document.getElementById('stomp-status').innerHTML = "It has now successfully connected to a stomp server serving price updates for some foreign exchange currency pairs."
}

client.connect({}, function() {
	this.subscribe("/fx/prices", function(data){
		eventHandler.dispatch('wsData', data.body)
	});
	
}, function(error) {
  alert(error.headers.message)
})

setInterval(function(){
table.updateSparkingData();
}, 30000)
const exampleSparkline = document.getElementById('example-sparkline')
Sparkline.draw(exampleSparkline, [1, 2, 3, 6, 8, 20, 2, 2, 4, 2, 3])